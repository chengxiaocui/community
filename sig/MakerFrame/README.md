
## MakerFrame-SIG组

## 工作目标

MakerFrame SIG组（MakerFrame Special Interest Group，简称MakerFrame SIG）
虽然开放麒麟等国产化操作系统已经开始大范围普及，但软件生态仍然处于很匮乏的状态，尤其是娱乐和框架引擎方面的应用更是少之又少，我们MakerFrame SIG组负责为开放麒麟开发简单高效的框架引擎，致力于让专业人士和非专业人士都来开发跨平台的应用和游戏，为国产化系统打出第一个游戏引擎的旗帜，大力促进开放麒麟的推广。

## SIG成员

+ owner
  - 深林孤鹰(leamus@qq.com)  Gitee ID Leamus

## SIG维护包列表

- maker-frame

## 邮件列表
