# 零信任SIG
零信任兴趣小组（Zero Trust, SIG) 提供一个零信任的技术交流环境，主要是对零信任架构和规范的解读，并提供openkylin现有的功能在零信任产品中的应用规范与接口，收集零信任产品对openkylin的需求期望。

## 工作目标

我们的工作目标主要是标准规范的研究以及零信任技术方案的探讨：  

- 零信任标准和规范研究：

  - 零信任标准和规范：对现有的国内外零信任相关标准和架构研究，从零信任理念、零信任产品架构方面，解读各厂家对零信任不同的实现以及共同的特性

  - 操作系统视角的零信任架构：当前国内主要的零信任模型中，更多的是网络层面的安全设计，可以从操作系统安全和功能层面，对零信任方案做补充完善

- 现有安全产品和技术与零信任的结合方案
  - 主要在用户认证、终端安全和数据安全等方面的功能产品与零信任结合

## SIG成员

### SIG-owner

- 杨诏钧
- 付焕章

### SIG-maintainers

- @fluyhz(fuhuanzhang@kylinos.cn)
- @kylinmy(mengyuan@kylinos.cn)
- @rjhd(zhouming@kylinos.cn)
- @yuanzhiroc(yuanzhipeng@kylinos.cn)
- @kylin-liangjianhao(liangjianhao@kylinos.cn)