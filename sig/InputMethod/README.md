## 输入法SIG组

### 工作目标

&emsp;&emsp;输入法SIG组是由一群热爱开源技术的爱好者所组成的团队。我们致力于为开源操作系统开发高质量的输入法，并在开源社区中积极推广和分享相关技术。   
&emsp;&emsp; SIG组欢迎广大开源爱好者积极参与进来，为开源输入法框架和开源输入法的发展和完善贡献自己的力量。同时，SIG组也将不断探索和尝试新的技术，为用户提供更加便捷、高效的输入体验。

### SIG成员
- hanteng@kylinos.cn
- liulinsong@kylinos.cn
- linyuxuan@kylinos.cn
- zhaokexin@kylinos.cn
- ningsiguang@kylinos.cn
- pangyi@kylinos.cn

### SIG维护包列表
- fcitx
- fcitx-qt5
- fcitx-configtool
- im-config
- fcitx5
- fcitx5-qt
- fcitx5-gtk
- fcitx5-configtool
- fcitx5-chinese-addons
- fcitx5-rime
- kylin-virtual-keyboard 
- gb-fonts
- fcitx5-quwei
- fcitx5-gb18030
- librime
- brise
- ok-input-method
- fontconfig
- fcitx5-extra-dbus

